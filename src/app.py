from flask import Flask, render_template, request
from flask_migrate import Migrate

from app_blog.models import Post
from app_blog.utils import blog_blueprint
from app_user.utils import admin_blueprint, is_active
from utils import alchemy_base

app = Flask(__name__)

app.config.from_json(filename='config.json')
alchemy_base.init_app(app)
migrate_obj = Migrate(app, alchemy_base)

app.register_blueprint(admin_blueprint)
app.register_blueprint(blog_blueprint)


@app.route('/')
def index():
    page = request.args.get('page', default=1, type=int)
    posts = list(
            Post.query.order_by(Post.pid.desc()).offset(
                    (page - 1) * 10 if page > 1 else page - 1).limit(10))
    return render_template('index.html', title='Home Page',
                           posts=posts, active=is_active(), page=page,
                           post_count=Post.query.count())
