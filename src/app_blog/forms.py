from flask_wtf import FlaskForm
from wtforms import SubmitField, TextField, TextAreaField, FileField
from wtforms.validators import DataRequired


class PostForm(FlaskForm):
    title = TextField(validators=[DataRequired()])
    picture = FileField()
    content = TextAreaField(validators=[DataRequired()])
    keywords = TextField(validators=[DataRequired()])
    submit = SubmitField(label='submit')


class CommentForm(FlaskForm):
    writer_name = TextField(validators=[DataRequired()])
    comment_content = TextAreaField(validators=[DataRequired()])
    submit = SubmitField(label='submit')
