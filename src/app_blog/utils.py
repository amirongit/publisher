from flask import Blueprint
from datetime import datetime

from utils import alchemy_base

from .models import Post, KeyWord

blog_blueprint = Blueprint('blog_blueprint', __name__, url_prefix='/blog/')


def generate_slug(title):
    date = str(datetime.now()).split()[0]
    slug_title = '-'.join(title.split())
    return date + '-' + slug_title


def handle_keywords(keywords):
    given_keys = [key.strip() for key in keywords.split(',')]
    valid_keys = list()
    for key in given_keys:
        key_query = KeyWord.query.filter(KeyWord.kword == key).first()
        if key_query is None:
            new_key = KeyWord(kword=key)
            alchemy_base.session.add(new_key)
            alchemy_base.session.commit()
            key_query = KeyWord.query.filter(KeyWord.kword == key).first()
            valid_keys.append(key_query)
        else:
            valid_keys.append(key_query)
    return valid_keys
