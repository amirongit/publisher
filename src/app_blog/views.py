from flask import (request, render_template,
                   flash, redirect, url_for,
                   abort, session)
from sqlalchemy.exc import IntegrityError
from uuid import uuid4
from werkzeug.utils import secure_filename

from app_user.models import SuperUser
from app_user.utils import is_active, login_required
from utils import alchemy_base

from .forms import PostForm, CommentForm
from .models import Post, KeyWord, Comment
from .utils import blog_blueprint, generate_slug, handle_keywords


@login_required
def new_post():
    form = PostForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            if len(form.title.data) < 64:
                try:
                    user = SuperUser.query.filter(
                            SuperUser.uid == session['user_id']).first()
                    new_post = Post(ptitle=form.title.data,
                                    pcontent=form.content.data,
                                    pslug=generate_slug(form.title.data))
                    user.user_posts.append(new_post)
                    new_post.post_keywords = handle_keywords(
                            form.keywords.data)
                    if form.picture.data:
                        new_post.ppname = str(
                            uuid4()) + form.picture.data.filename
                    alchemy_base.session.add(new_post)
                    alchemy_base.session.commit()
                    if new_post.ppname:
                        form.picture.data.save('static/PostImages/' +
                                               new_post.ppname)
                    flash('New post added successfully!', category='success')
                    return redirect(url_for('index'))
                except IntegrityError:
                    alchemy_base.session.rollback()
                    flash('Something went wrong!', category='warning')
                    return render_template('app_blog/post.html', form=form,
                                           title='New Post', active=True,
                                           target=url_for(
                                               'blog_blueprint.new_post'))
            else:
                flash('Title must be less than 64 characters!',
                      category='warning')
                return render_template('app_blog/post.html', form=form,
                                       title='New Post', active=True,
                                       target=url_for(
                                           'blog_blueprint.new_post'))
        else:
            flash('Something went wrong!', category='warning')
            return render_template('app_blog/post.html', form=form,
                                   title='New Post', active=True,
                                   target=url_for('blog_blueprint.new_post'))
    else:
        return render_template('app_blog/post.html', form=form,
                               title='New Post', active=True,
                               target=url_for('blog_blueprint.new_post'))


blog_blueprint.add_url_rule('/new_post/', 'new_post',
                            new_post, methods=['GET', 'POST'])


@blog_blueprint.route('/display_post/<slug>/')
def display_post(slug):
    post = Post.query.filter(Post.pslug == slug).first()
    form = CommentForm()
    if post is not None:
        return render_template('app_blog/display_post.html', title=post.ptitle,
                               post=post, active=is_active(), form=form)
    else:
        abort(404)


@blog_blueprint.route('/new_comment/', methods=['POST'])
def new_comment():
    pid = request.args.get('pid', type=str)
    form = CommentForm()
    post = Post.query.get(pid)
    if form.validate_on_submit():
        new_comment = Comment(cwname=form.writer_name.data,
                              ccontent=form.comment_content.data)
        post.post_comments.append(new_comment)
        alchemy_base.session.commit()
        flash('Your comment submited successfully!', category='success')
        return redirect(url_for('blog_blueprint.display_post',
                        slug=post.pslug))
    else:
        flash('Something went wrong!', category='warning')
        return redirect(url_for('blog_blueprint.display_post',
                        slug=post.pslug))


@blog_blueprint.route('/user_posts/<uname>')
def user_posts(uname):
    uid = SuperUser.query.filter(SuperUser.uname == uname).first().uid
    posts = Post.query.filter(Post.writer_id == uid).all()
    return render_template('app_blog/list_posts.html', posts=posts,
                           title=f'{posts[0].post_writer.uname}\'s posts',
                           active=is_active())


@blog_blueprint.route('/keywords_posts/<key>')
def keyword_posts(key):
    keyword = KeyWord.query.filter(
            KeyWord.kword == key).first()
    posts = keyword.keyword_posts
    return render_template('app_blog/list_posts.html', posts=posts,
                           title=keyword.kword, active=is_active())


@login_required
def delete_post():
    post = Post.query.get(request.args.get('pid', type=int))
    alchemy_base.session.delete(post)
    alchemy_base.session.commit()
    flash('Post removed successfully!', category='success')
    return redirect(url_for('index'))


blog_blueprint.add_url_rule('/delete_post/', 'delete_post', delete_post)


@login_required
def edit_post():
    post = Post.query.get(request.args.get('pid', type=int))
    if request.method == 'GET':
        form = PostForm(obj=post)
        form.title.data = post.ptitle
        form.keywords.data = ', '.join(
                [key.kword for key in post.post_keywords])
        form.content.data = post.pcontent
        return render_template('app_blog/post.html', form=form,
                               title='Edit Post', active=True,
                               target=url_for('blog_blueprint.edit_post',
                                              pid=post.pid))
    else:
        form = PostForm()
        try:
            post.ptitle = form.title.data
            if form.picture.data:
                post.ppname = str(uuid4()) + form.picture.data.filename
                form.picture.data.save(f'static/PostImages/{post.ppname}')
            post.post_keywords = handle_keywords(form.keywords.data)
            post.pcontent = form.content.data
            alchemy_base.session.commit()
            flash('Post edited successfully!', category='success')
            return redirect(url_for('blog_blueprint.display_post',
                                    slug=post.pslug))
        except IntegrityError:
            alchemy_base.session.rollback()
            flash('Something went wrong!', category='warning')
            return render_template('app_blog/post.html', form=form,
                                   title='Edit Post', active=True,
                                   target=url_for('blog_blueprint.edit_post',
                                                  pid=post.pid))


blog_blueprint.add_url_rule('/edit_post/', 'edit_post',
                            edit_post, methods=['GET', 'POST'])


@login_required
def answer_comment():
    comment = Comment.query.get(request.args.get('cid', type=int))
    form = CommentForm()
    comment.canswer = form.comment_content.data
    alchemy_base.session.commit()
    flash('Your answer submited successfully!', category='success')
    return redirect(url_for('blog_blueprint.display_post',
                    slug=comment.comment_post.pslug))


blog_blueprint.add_url_rule('/answer_comment/', 'answer_comment',
                            answer_comment, methods=['POST'])
