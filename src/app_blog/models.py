from sqlalchemy import String, Text, Integer, Column, ForeignKey, Table
from sqlalchemy.orm import relationship

from utils import alchemy_base

associate_post_keyword = Table('associate_post_keyword', alchemy_base.metadata,
                               Column('pid', Integer,
                                      ForeignKey('posts.pid')),
                               Column('kid', Integer,
                                      ForeignKey('keywords.kid')))


class Post(alchemy_base.Model):

    __tablename__ = 'posts'

    pid = Column(Integer, primary_key=True)
    ptitle = Column(String(64), nullable=False, unique=True)
    pslug = Column(String(128), nullable=False, unique=True)
    ppname = Column(String(64))
    pcontent = Column(Text, nullable=False)
    writer_id = Column(Integer, ForeignKey('superusers.uid'))
    post_writer = relationship('SuperUser', back_populates='user_posts')
    post_keywords = relationship('KeyWord', secondary=associate_post_keyword,
                                 back_populates='keyword_posts')
    post_comments = relationship('Comment', back_populates='comment_post')


class Comment(alchemy_base.Model):

    __tablename__ = 'comments'

    cid = Column(Integer, primary_key=True)
    cwname = Column(String(64), nullable=False)
    ccontent = Column(String(512), nullable=False)
    canswer = Column(String(512), nullable=True)
    post_id = Column(Integer, ForeignKey('posts.pid'))
    comment_post = relationship('Post', back_populates='post_comments')


class KeyWord(alchemy_base.Model):

    __tablename__ = 'keywords'

    kid = Column(Integer, primary_key=True)
    kword = Column(String(16), unique=True, nullable=False)
    keyword_posts = relationship('Post', secondary=associate_post_keyword,
                                 back_populates='post_keywords')
