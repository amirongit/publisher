from flask import Blueprint, session
from flask import abort, session

admin_blueprint = Blueprint('admin_blueprint', __name__, url_prefix='/dash/')


def is_active():
    if session.get('user_id') is not None:
        return True
    else:
        return False


def login_required(endpoint):
    def decorator(*args, **kwargs):
        if session.get('user_id') is None:
            abort(403)
        return endpoint(*args, **kwargs)
    return decorator
