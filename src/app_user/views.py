from flask import (request, render_template,
                   flash, session, redirect, url_for)

from utils import alchemy_base

from .forms import LoginForm
from .models import SuperUser
from .utils import admin_blueprint, login_required


@admin_blueprint.route('/login/', methods=['GET', 'POST'])
def login():
    if session.get('user_id') is not None:
        flash('You are already logged in!', category='warning')
        return redirect(url_for('index'))
    form = LoginForm(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            user = SuperUser.query.filter(SuperUser.uemail ==
                                          form.email.data).first()
            if user is not None:
                if user.passwd_check(form.password.data):
                    session['user_id'] = user.uid
                    session['user_name'] = user.uname
                    flash('You have logged in!', category='success')
                    return redirect(url_for('index'))
                else:
                    flash('Wrong email/password!', category='warning')
                    return render_template('app_user/login.html', form=form,
                                           title='Login')
            else:
                flash('Wrong email/password!', category='warning')
                return render_template('app_user/login.html', form=form,
                                       title='Login')
        else:
            flash('Something went wrong!', category='warning')
            return render_template('app_user/login.html', form=form,
                                   title='Login')
    else:
        return render_template('app_user/login.html', form=form, title='Login')


@login_required
def logout():
    session.clear()
    flash('You have logged out!', category='success')
    return redirect(url_for('index'))


admin_blueprint.add_url_rule('/logout/', 'logout', logout)
