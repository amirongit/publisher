from werkzeug.security import check_password_hash
from sqlalchemy import String, Integer, Column
from sqlalchemy.orm import relationship

from utils import alchemy_base


class SuperUser(alchemy_base.Model):

    __tablename__ = 'superusers'

    uid = Column(Integer, primary_key=True)
    upasswd = Column(String(128), nullable=False)
    uname = Column(String(16), unique=True, nullable=False)
    uemail = Column(String(128), unique=True, nullable=False)
    user_posts = relationship('Post', back_populates='post_writer')

    def passwd_check(self, passwd):
        return check_password_hash(self.upasswd, passwd)
